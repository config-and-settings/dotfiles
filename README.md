# .dotfiles

A repository to keep my dotfiles in sync. Super handy if you purchase a new computer!.

I'm using [this technique](https://www.atlassian.com/git/tutorials/dotfiles) for dotfiles (so I avoid having so many symlinks), and [this one](https://pawelgrzybek.com/sync-vscode-settings-and-snippets-via-dotfiles-on-github/) for syncing VS Code Settings on the same (this one does use symlinks).

Enjoy!